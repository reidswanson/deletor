examples package
================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   examples.attn
   examples.gsf
   examples.mlp

Submodules
----------

examples.build\_tfrecords module
--------------------------------

.. automodule:: examples.build_tfrecords
   :members:
   :undoc-members:
   :show-inheritance:

examples.pipeline module
------------------------

.. automodule:: examples.pipeline
   :members:
   :undoc-members:
   :show-inheritance:

examples.utils module
---------------------

.. automodule:: examples.utils
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: examples
   :members:
   :undoc-members:
   :show-inheritance:
