test package
============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   test.examples
   test.random
   test.ranking

Submodules
----------

test.test\_losses module
------------------------

.. automodule:: test.test_losses
   :members:
   :undoc-members:
   :show-inheritance:

test.test\_metrics module
-------------------------

.. automodule:: test.test_metrics
   :members:
   :undoc-members:
   :show-inheritance:

test.test\_preprocessing module
-------------------------------

.. automodule:: test.test_preprocessing
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: test
   :members:
   :undoc-members:
   :show-inheritance:
