deletor package
===============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   deletor.math
   deletor.models
   deletor.random
   deletor.ranking

Submodules
----------

deletor.constants module
------------------------

.. automodule:: deletor.constants
   :members:
   :undoc-members:
   :show-inheritance:

deletor.losses module
---------------------

.. automodule:: deletor.losses
   :members:
   :undoc-members:
   :show-inheritance:

deletor.metrics module
----------------------

.. automodule:: deletor.metrics
   :members:
   :undoc-members:
   :show-inheritance:

deletor.preprocessing module
----------------------------

.. automodule:: deletor.preprocessing
   :members:
   :undoc-members:
   :show-inheritance:

deletor.tfutils module
----------------------

.. automodule:: deletor.tfutils
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: deletor
   :members:
   :undoc-members:
   :show-inheritance:
