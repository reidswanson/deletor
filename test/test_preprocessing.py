"""
Documentation
"""
# Python Modules
import unittest

# 3rd Party Modules
import numpy as np
import tensorflow as tf

import sklearn.preprocessing as skpp

# Project Modules
import deletor.preprocessing as lwpp


# noinspection DuplicatedCode
class TestPreprocessing(unittest.TestCase):
    x = np.array([[2,  0,  0,  9,  8,  2, -1,  1],
                  [2,  2,  7,  8,  7,  0,  4, -2],
                  [5,  0,  6,  3,  8,  7,  8,  7],
                  [2, -1,  7,  3,  1,  6,  4,  5],
                  [1,  7,  8,  2,  1,  1,  3,  7]], dtype=np.float32)

    def test_min_max_scaler(self):
        skscaler = skpp.MinMaxScaler()
        skscaler.fit(self.x)
        lwscaler = lwpp.MinMaxScaler(
            skscaler.min_,
            skscaler.scale_,
            skscaler.data_min_,
            skscaler.data_max_,
            skscaler.data_range_,
            skscaler.n_samples_seen_
        )

        exp_x = skscaler.transform(self.x)
        act_x = lwscaler.transform(tf.constant(self.x))
        np.testing.assert_array_almost_equal(act_x, exp_x)

        exp_inv_x = self.x
        act_inv_x = lwscaler.inverse_transform(act_x)

        np.testing.assert_array_almost_equal(act_inv_x, exp_inv_x)

    def test_standard_scaler(self):
        skscaler = skpp.StandardScaler()
        skscaler.fit(self.x)
        lwscaler = lwpp.StandardScaler(
            skscaler.scale_,
            skscaler.mean_,
            skscaler.var_,
            skscaler.n_samples_seen_,
            skscaler.with_mean,
            skscaler.with_std
        )

        exp_x = skscaler.transform(self.x)
        act_x = lwscaler.transform(tf.constant(self.x))
        np.testing.assert_array_almost_equal(act_x, exp_x)

        exp_inv_x = self.x
        act_inv_x = lwscaler.inverse_transform(act_x)
        np.testing.assert_array_almost_equal(act_inv_x, exp_inv_x)

    def test_robust_scaler(self):
        skscaler = skpp.RobustScaler()
        skscaler.fit(self.x)
        lwscaler = lwpp.RobustScaler(
            skscaler.center_,
            skscaler.scale_,
            skscaler.with_centering,
            skscaler.with_scaling
        )

        exp_x = skscaler.transform(self.x)
        act_x = lwscaler.transform(tf.constant(self.x))
        np.testing.assert_array_almost_equal(act_x, exp_x)

        exp_inv_x = self.x
        act_inv_x = lwscaler.inverse_transform(act_x)
        np.testing.assert_array_almost_equal(act_inv_x, exp_inv_x)

    def test_power_transformer(self):
        skscaler = skpp.PowerTransformer()
        skscaler.fit(self.x)
        lwscaler = lwpp.PowerTransformer(skscaler.lambdas_, skscaler._scaler)

        exp_x = skscaler.transform(self.x)
        act_x = lwscaler.transform(tf.constant(self.x))
        np.testing.assert_array_almost_equal(act_x, exp_x, decimal=5)

        exp_inv_x = self.x
        act_inv_x = lwscaler.inverse_transform(act_x)
        np.testing.assert_array_almost_equal(act_inv_x, exp_inv_x, decimal=5)

        skscaler = skpp.PowerTransformer(standardize=False)
        skscaler.fit(self.x)
        lwscaler = lwpp.PowerTransformer(skscaler.lambdas_)
        exp_x = skscaler.transform(self.x)
        act_x = lwscaler.transform(tf.constant(self.x))
        np.testing.assert_array_almost_equal(act_x, exp_x, decimal=5)

        exp_inv_x = self.x
        act_inv_x = lwscaler.inverse_transform(act_x)
        np.testing.assert_array_almost_equal(act_inv_x, exp_inv_x, decimal=5)
