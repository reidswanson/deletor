"""
Documentation
"""
# Python Modules
import unittest

# 3rd Party Modules

# Project Modules
import deletor.models.mlp as models


class TestModels(unittest.TestCase):
    def test_simple_scoring_network(self):
        model_params = {
            models.ModelParameter.RANDOM_SEED: 0,
            models.ModelParameter.N_UNITS: [8, 4]
        }
        model = models.SimpleScoringNetwork(model_params)
