"""
Documentation
"""
# Python Modules

# 3rd Party Modules
import numpy as np

# Project Modules


EPSILON = 1e-10
MIN_FLOAT_32 = np.finfo(np.float32).min
