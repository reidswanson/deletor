#
#
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#

# Python Modules
from typing import Any, Dict, List

# 3rd Party Modules
import tensorflow as tf

# Project Modules
import deletor.models.gsf as gsf


class ModelParameter(gsf.ModelParameter):
    N_SAMPLED_FEATURES = 'n_sampled_features'


class WeakGroupwiseScoringNetwork(gsf.GroupwiseScoringNetwork):
    def __init__(self, model_parameters: Dict[str, Any]):
        n_features = model_parameters[ModelParameter.N_FEATURES]
        n_sampled_features = model_parameters[ModelParameter.N_SAMPLED_FEATURES]
        random_seed = model_parameters.get(ModelParameter.RANDOM_SEED)

        # Select a subset of features that this weak model will use
        self.feature_indices = tf.range(n_features)
        self.feature_indices = tf.random.shuffle(self.feature_indices, random_seed)
        self.feature_indices = self.feature_indices[:n_sampled_features]

        # Update the model parameters based to the base GSF model so that the
        # number of features reflects the sampled features and not the full
        # set.
        model_parameters[ModelParameter.N_FEATURES] = n_sampled_features

        super().__init__(model_parameters)


class EnsembleScoringNetwork(gsf.GroupwiseScoringNetwork):
    def __init__(
            self,
            model_parameters: Dict[str, Any],
            weak_models: List[WeakGroupwiseScoringNetwork]
    ):
        super().__init__(model_parameters)

        self.weak_models = weak_models

    def apply_weak_models(self, x, y):
        for weak_model in self.weak_models:
            yp = weak_model(x, training=False)
