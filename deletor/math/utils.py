# Python Modules

# 3rd Party Modules
import tensorflow as tf

# Project Modules


# noinspection PyTypeChecker
def spacing(x: tf.Tensor):
    """
    Implements the equivalent of
    `numpy.spacing <https://docs.scipy.org/doc/numpy/reference/generated/numpy.spacing.html>`_

    :param x:
    :return:
    """
    x += 1
    before = tf.math.nextafter(x, x - 1)
    after = tf.math.nextafter(x, x + 1)

    return tf.math.minimum(x - before, after - x)


def approx_abs(x):
    # See this comment:
    # https://math.stackexchange.com/questions/1172472/differentiable-approximation-of-the-absolute-value-function#comment2389016_1172480
    x = tf.cast(x, tf.float32)

    # return tf.math.log1p(tf.math.exp(2*x)) - x
    return tf.math.sqrt(tf.math.square(x))
