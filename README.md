# Deletor

Deletor is a toolkit for developing learning to rank algorithms using deep neural networks.
It is similar to [tensorflow_ranking](https://github.com/tensorflow/ranking) (and borrows some code from it), but it is implemented using [TensorFlow 2](https://www.tensorflow.org/), which is generally easier to work with.

## Install
The project is not currently on PyPI.
The easiest way to use or install the project is to clone it or via pip using its native Git support:

``` bash
>pip install git+https://bitbucket.org/reidswanson/deletor.git
```

## Performance
The performance of the 3 primary models implemented in this toolkit, using an approximate NDCG loss, is given in the table below.
These results are in line with the reported values in current state of the art neural network ranking algorithms.

| Model | NDCG@1 | NDCG@5 | NDCG@10 |
|-------|--------|--------|---------|
| MLP   | 45.96  | 44.30  | 45.31   |
| GSF   | 46.38  | 45.63  | 47.52   |
| GASF  | 44.46  | 43.97  | 45.94   | 

## Documentation
See the full documentation on [Read the Docs](https://deletor.readthedocs.io/en/latest/).

## License
Any file without a specific copyright notification is released using the Apache v2 License by me (Reid Swanson).
See the [LICENSE](LICENSE) file for more details.
Some files have been adapted from other projects and should have specific license notifications with proper attribution at the top of the file.
 